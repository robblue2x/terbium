import uuidv4 from 'uuid/v4';
import store from './store';

const EventEmitter = require('events');

class ToDos extends EventEmitter {
  constructor() {
    super();
    this.list = store.load('list', [
      {
        id: uuidv4(),
        notes:
          `welcome to terbium todo list!

terbium stores your todo list in your browsers local storage, so your data stays on your computer

click the blue button at the top to create a new task

click the green button on a task to send it to the top of your list
click the orange button on a task to send it to the bottom of your list
finally click the red button on a task to delete it

if you put a url/link in your notes terbium will provide a quick link at the bottom of the task,` +
          ` this will open your link in a new tab
for example: https://robblue2x.gitlab.io/terbium
`
      }
    ]);

    store.onchange(l => {
      this.list = l;
      this.emit('update', [...this.list]);
    });
  }

  onChange() {
    this.emit('update', [...this.list]);
    store.save('list', this.list);
  }

  get() {
    return [...this.list];
  }

  add({ title, notes, source }) {
    this.list.unshift({ id: uuidv4(), title, notes, source });
    this.onChange();
  }

  remove(id) {
    this.list = this.list.filter(a => a.id !== id);
    this.onChange();
  }

  update({ id, notes, source }) {
    const found = this.list.find(a => a.id === id);
    if (!found) return;
    found.notes = notes;
    found.source = source;
    this.onChange();
  }

  top(id) {
    const found = this.list.find(a => a.id === id);
    if (!found) return;
    this.list = this.list.filter(a => a.id !== id);
    this.list.unshift(found);
    this.onChange();
  }

  bottom(id) {
    const found = this.list.find(a => a.id === id);
    if (!found) return;
    this.list = this.list.filter(a => a.id !== id);
    this.list.push(found);
    this.onChange();
  }
}

export default new ToDos();
