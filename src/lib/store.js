exports.save = (key, data) => {
  const d = { data };
  localStorage.setItem(key, JSON.stringify(d));
};

exports.load = (key, def) => {
  try {
    const loaded = JSON.parse(localStorage.getItem(key));
    if (loaded) {
      return loaded.data;
    }
  } catch (ex) {
    console.error(ex.message);
  }

  return def;
};

exports.onchange = f => {
  window.onstorage = e => {
    try {
      const loaded = JSON.parse(e.newValue);
      f(loaded.data);
    } catch (ex) {
      console.error(ex.message);
    }
  };
};
