const path = require('path');
const webpack = require('webpack');
const fs = require('fs');
const assert = require('assert');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');

const { devPort } = JSON.parse(fs.readFileSync('./package.json'));
assert(devPort, 'No devPort specified in package.json');

module.exports = merge(common, {
  devtool: 'inline-source-map',
  mode: 'development',
  devServer: {
    port: devPort,
    contentBase: path.join(__dirname, 'public'),
    watchContentBase: true,
    historyApiFallback: true
  },

  plugins: [new webpack.NamedModulesPlugin()]
});
